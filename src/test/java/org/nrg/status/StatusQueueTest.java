/*
 * org.nrg.status.StatusQueueTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/2/13 12:20 PM
 */
package org.nrg.status;

import static org.junit.Assert.*;

import java.util.NoSuchElementException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nrg.status.StatusMessage.Status;

public class StatusQueueTest {
    private static final StatusMessage sm1 = new StatusMessage(StatusQueueTest.class, Status.PROCESSING, "foo");
    private static final StatusMessage sm2 = new StatusMessage(StatusQueueTest.class, Status.PROCESSING, "bar");

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for {@link org.nrg.status.StatusQueue#notify(org.nrg.status.StatusMessage)}.
     */
    @Test
    public void testNotifyStatusMessage() {
        final StatusQueue sq = new StatusQueue();
        assertNull(sq.peek());
        sq.notify(sm1);
        assertSame(sm1, sq.peek());
    }

    /**
     * Test method for {@link org.nrg.status.StatusQueue#peek()}.
     */
    @Test
    public void testPeek() {
        final StatusQueue sq = new StatusQueue();
        assertNull(sq.peek());
        sq.notify(sm1);
        assertSame(sm1, sq.peek());
        sq.notify(sm2);
        assertSame(sm1, sq.peek());
        assertSame(sm1, sq.remove());
        assertSame(sm2, sq.peek());
        assertSame(sm2, sq.remove());
        assertNull(sq.peek());
    }

    /**
     * Test method for {@link org.nrg.status.StatusQueue#poll()}.
     */
    @Test
    public void testPoll() {
        final StatusQueue sq = new StatusQueue();
        assertNull(sq.poll());
        sq.notify(sm1);
        sq.notify(sm2);
        assertSame(sm1, sq.poll());
        assertSame(sm2, sq.poll());
        assertNull(sq.poll());
    }

    /**
     * Test method for {@link org.nrg.status.StatusQueue#remove()}.
     */
    @Test
    public void testRemove() {
        final StatusQueue sq = new StatusQueue();
        sq.notify(sm1);
        sq.notify(sm2);
        assertSame(sm1, sq.remove());
        assertSame(sm2, sq.remove());
    }

    @Test(expected=NoSuchElementException.class)
    public void testRemoveEmpty() {
        final StatusQueue sq = new StatusQueue();
        sq.remove();
    }
    
    @Test(expected=NoSuchElementException.class)
    public void testRemoveToEmpty() {
        final StatusQueue sq = new StatusQueue();
        sq.notify(sm1);
        sq.notify(sm2);
        assertSame(sm1, sq.remove());
        assertSame(sm2, sq.remove());
        sq.remove();
    }
    
    /**
     * Test method for {@link org.nrg.status.StatusQueue#clear()}.
     */
    @Test
    public void testClear() {
        final StatusQueue sq = new StatusQueue();
        sq.notify(sm1);
        sq.notify(sm2);
        assertSame(sm1, sq.peek());
        sq.clear();
        assertNull(sq.peek());
    }
}
