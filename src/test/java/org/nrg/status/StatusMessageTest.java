/*
 * org.nrg.status.StatusMessageTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/2/13 12:20 PM
 */
package org.nrg.status;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nrg.status.StatusMessage.Status;

public class StatusMessageTest {

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for {@link org.nrg.status.StatusMessage#StatusMessage(java.lang.Object, org.nrg.status.StatusMessage.Status, java.lang.CharSequence)}.
     */
    @Test
    public void testStatusMessage() {
        final Object source = new Object();
        new StatusMessage(source, null, null);
        new StatusMessage(source, Status.COMPLETED, null);
        new StatusMessage(source, null, "foo");
        new StatusMessage(source, Status.FAILED, "baz");
    }

    @Test(expected=IllegalArgumentException.class)
    public void testStatusMessageNullSource() {
        new StatusMessage(null, Status.FAILED, "foo");
    }

    /**
     * Test method for {@link org.nrg.status.StatusMessage#getStatus()}.
     */
    @Test
    public void testGetStatus() {
        final Object source = new Object();
        final StatusMessage m0 = new StatusMessage(source, null, null);
        assertNull(m0.getStatus());
        final StatusMessage m1 = new StatusMessage(source, Status.COMPLETED, null);
        assertSame(Status.COMPLETED, m1.getStatus());
    }

    /**
     * Test method for {@link org.nrg.status.StatusMessage#getMessage()}.
     */
    @Test
    public void testGetMessage() {
        final Object source = new Object();
        final StatusMessage m0 = new StatusMessage(source, null, null);
        assertNull(m0.getMessage());
        final StatusMessage m1 = new StatusMessage(source, null, "foo");
        assertEquals("foo", m1.getMessage());
    }
}
