/*
 * org.nrg.status.StatusProducer
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/2/13 12:20 PM
 */
package org.nrg.status;

import org.nrg.status.StatusMessage.Status;
import static org.nrg.status.StatusMessage.Status.*;

@Deprecated
public class StatusProducer extends BasicStatusPublisher {
	private final Object control;

	/**
	 * 
	 */
	public StatusProducer(final Object control) {
		this.control = control;
	}

	/**
	 * @param listeners
	 */
	public StatusProducer(final Object control, Iterable<StatusListenerI> listeners) {
		super(listeners);
		this.control = control;
	}

	/**
	 * @param listeners
	 */
	public StatusProducer(final Object control, StatusListenerI... listeners) {
		super(listeners);
		this.control = control;
	}

	protected final void report(final Status status, final String message) {
		publish(new StatusMessage(control, status, message));
	}

	protected final void processing(final String message) {
		report(PROCESSING, message);
	}

	protected final void warning(final String message) {
		report(WARNING, message);
	}

	protected final void failed(final String message) {
		report(FAILED, message);
	}

	protected final void completed(final String message) {
		report(COMPLETED, message);
	}
}
