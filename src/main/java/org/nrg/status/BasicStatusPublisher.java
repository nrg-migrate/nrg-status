/*
 * org.nrg.status.BasicStatusPublisher
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/2/13 12:20 PM
 */
package org.nrg.status;

import java.io.Closeable;
import java.util.Collections;
import java.util.Set;
import java.util.LinkedHashSet;

import org.nrg.status.StatusListenerI;
import org.nrg.status.StatusMessage;
import org.nrg.status.StatusProducerI;

public class BasicStatusPublisher implements StatusProducerI,Closeable {
    private final Set<StatusListenerI> listeners = new LinkedHashSet<StatusListenerI>();

    public BasicStatusPublisher() {}

    public BasicStatusPublisher(final Iterable<StatusListenerI> listeners) {
        for (final StatusListenerI listener : listeners) {
            this.addStatusListener(listener);
        }
    }

    public BasicStatusPublisher(final StatusListenerI...listeners) {
        for (final StatusListenerI listener : listeners) {
            this.addStatusListener(listener);
        }
    }

    /* (non-Javadoc)
     * @see org.nrg.StatusProducerI#addStatusListener(org.nrg.StatusListener)
     */
    public final void addStatusListener(final StatusListenerI l) {
        listeners.add(l);
    }

    /* (non-Javadoc)
     * @see org.nrg.StatusProducerI#removeStatusListener(org.nrg.StatusListener)
     */
    public final void removeStatusListener(final StatusListenerI l) {
        listeners.remove(l);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.status.StatusPublisherI#publish(org.nrg.status.StatusMessage)
     */
    public final void publish(final StatusMessage m) {
        for (final StatusListenerI l : listeners) {
            l.notify(m);
        }
    }

    public final Set<StatusListenerI> getListeners() {
        return Collections.unmodifiableSet(listeners);
    }

    /*
     * (non-Javadoc)
     * @see java.io.Closeable#close()
     */
    public void close() {
        listeners.clear();
    }
}
