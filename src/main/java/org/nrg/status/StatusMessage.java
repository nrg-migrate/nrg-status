/*
 * org.nrg.status.StatusMessage
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/2/13 12:20 PM
 */
package org.nrg.status;

import java.util.EventObject;

public class StatusMessage extends EventObject {
	private static final long serialVersionUID = 1L;
	public static enum Status { PROCESSING, WARNING, FAILED, COMPLETED };

	private final Status status;
	private final CharSequence message;

	public StatusMessage(final Object source, final Status status, final CharSequence message) {
		super(source);
		this.status = status;
		this.message = message;
	}

	public Status getStatus() { return status; }

	public String getMessage() { return null == message ? null : message.toString(); }
}
